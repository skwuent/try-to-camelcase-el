;;; try-to-camelcase.el

;; Copyright (C) 2016 NGK Sternhagen

;; Author: NGK Sternhagen <sternhagen@protonmail.ch>
;; Version: 0.0
;; Keywords: CamelCase
;; URL: http://gitlab.com/skwuent/try-to-camelcase-el

;;; Commentary:

;; This package provides functions to try and change non-camelcase strings into
;; CamelCase strings


;;;###autoload
(defun try-to-camelcase-word ()
  "for the string at point, try to guess the words contained in
   the string and capitalize each of them"
  (interactive)
  (message "not implemented!"))

;;;###autoload
(defun try-to-camelcase-kebab ()
  "for the string at point, replace all substrings '-<x>' with the substring 'X'"
  (interactive)
  (message "not implemented!"))
